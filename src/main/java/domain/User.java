package domain;

public class User {
    private String name;
    private String username;
    private int id;
    private static int id_gen=0;

    public void generateId(){
        id=id_gen++;
    }
    public User(String name,String username){
        setName(name);
        setUsername(username);
    }

    public String getName() {
        return name;
    }

    public String getUsername() {
        return username;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    @Override
    public String toString(){
    return id + " " + name + " " + username;
}
}
